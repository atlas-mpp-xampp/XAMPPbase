#ifndef XAMPPbase_ChronoHandler_H
#define XAMPPbase_ChronoHandler_H

#include <memory>

#include "XAMPPbase/ActiveTimer.h"

/**
 * @brief Helper class to create and control timers
 */
class ChronoHandle {
 public:
  /**
   * @brief Construct the handle.
   * @param name The name of the handle
   */
  ChronoHandle(const std::string& name);

  /**
   * @brief Construct the handle.
   * @tparam T The type of this handle's parent
   * @param parent The parent of this handle
   * @param name The name of the handle
   *
   * If this version is chosen, the parent's name is prepended to the name
   * provided.
   */
  template <typename T>
  ChronoHandle(const T* parent, const std::string& name)
      : ChronoHandle(parent->name() + "." + name) {}

  /**
   * @brief Get this handle's ActiveTimer object
   *
   * So long as there is an instance of this handle's timer alive, another
   * reference to that object is returned, otherwise a new one is created and
   * returned. Each creation and destruction of an ActiveTimer corresponds to
   * a single start and stop call.
   */
  std::shared_ptr<ActiveTimer> activate();

  /**
   * @brief Get this handle's ChronoEntity object directly
   *
   * Due to the IChronoStatSvc's interface this object is necessarily returned
   * in the running state.
   */
  ChronoEntity* entity();

  /**
   * @brief Get this handle's ChronoEntity object directly
   *
   * Due to the IChronoStatSvc's interface this object is necessarily returned
   * in the running state.
   */
  ChronoEntity& operator*() { return *entity(); }

  /**
   * @brief Get this handle's ChronoEntity object directly
   *
   * Due to the IChronoStatSvc's interface this object is necessarily returned
   * in the running state.
   */
  ChronoEntity* operator->() { return entity(); }

 private:
  /// The weak pointer to this handle's active timer
  std::weak_ptr<ActiveTimer> m_activeTimer;
  /// The entity
  ChronoEntity* m_entity{nullptr};
  /// The name of this entity
  std::string m_name;
};  //> end class ChronoHandle

#endif  //> !XAMPPbase_ChronoHandler_H
