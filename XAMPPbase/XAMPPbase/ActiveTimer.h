#ifndef XAMPPbase_ActiveTimer_H
#define XAMPPbase_ActiveTimer_H

// Forward declares
class ChronoEntity;

/**
 * @brief Helper class for starting and stopping timers.
 *
 * When the class takes control of a timer it starts it, when the class is
 * destroyed or given a new timer it stops the one it currently controls.
 * This allows you to create an active timer in a function and know that no
 * matter how the function exits, that timer will be stopped.
 */
class ActiveTimer {
 public:
  /**
   * @brief Construct the timer.
   * @param timer The timer to start
   * @param held If this is set to a running timer, then that timer will be
   * stopped while this entry exists and restarted when this is destroyed.
   */
  ActiveTimer(ChronoEntity* timer = nullptr, ChronoEntity* held = nullptr);

  /**
   * @brief Destruct this object.
   *
   * This stops the active timer and restarts the held timer (if relevant).
   */
  ~ActiveTimer();

  /**
   * @brief Provide a new timer to control.
   * @param timer The new timer to control.
   * @return The previously held timer.
   *
   * The current active timer will be stopped and be replaced by the new one.
   */
  ChronoEntity* switchTimer(ChronoEntity* timer);

  /**
   * @brief Release control over the current timer.
   * @return The current active timer.
   *
   * The timer will not be stopped and this object will cease controlling it.
   */
  ChronoEntity* release();

  /// The currently controled timer
  ChronoEntity* timer() const;

  /// The currently held timer
  ChronoEntity* held() const;

 private:
  /// The currently controled timer
  ChronoEntity* m_timer;

  /// The currently held timer
  ChronoEntity* m_held;
};  //> end class ActiveTimer

#endif  //> !XAMPPbase_ActiveTimer_H
