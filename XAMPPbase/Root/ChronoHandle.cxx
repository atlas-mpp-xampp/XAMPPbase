#include "XAMPPbase/ChronoHandle.h"

#include <GaudiKernel/ChronoEntity.h>
#include <GaudiKernel/IChronoStatSvc.h>
#include <GaudiKernel/ServiceHandle.h>

ChronoHandle::ChronoHandle(const std::string& name) : m_name(name) {}

std::shared_ptr<ActiveTimer> ChronoHandle::activate() {
  if (m_activeTimer.expired()) {
    // We need to make a new one
    auto timer = std::make_shared<ActiveTimer>(entity());
    // Make the internal weak pointer point to this new object
    m_activeTimer = timer;
    return timer;
  } else
    // Otherwise return a new reference
    return m_activeTimer.lock();
}

ChronoEntity* ChronoHandle::entity() {
  if (!m_entity) {
    // We need to make the entity
    ServiceHandle<IChronoStatSvc> svc("ChronoStatSvc/ChronoStatSvc",
                                      "ChronoHandle");
    if (!svc.retrieve().isSuccess())
      throw std::runtime_error("Failed to retrieve ChronoStatSvc");
    m_entity = svc->chronoStart(m_name);
  } else
    m_entity->start();
  return m_entity;
}
