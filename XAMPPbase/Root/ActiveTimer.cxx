#include "XAMPPbase/ActiveTimer.h"

#include "GaudiKernel/ChronoEntity.h"

ActiveTimer::ActiveTimer(ChronoEntity* timer, ChronoEntity* held)
    : m_timer(timer), m_held(held) {
  if (m_timer)
    m_timer->start();
  if (m_held && m_held->status() == IChronoSvc::RUNNING)
    m_held->stop();
}

ActiveTimer::~ActiveTimer() {
  if (m_timer)
    m_timer->stop();
  if (m_held)
    m_held->start();
}

ChronoEntity* ActiveTimer::switchTimer(ChronoEntity* timer) {
  ChronoEntity* previous = m_timer;
  if (previous)
    previous->stop();
  m_timer = timer;
  if (m_timer)
    m_timer->start();
  return previous;
}

ChronoEntity* ActiveTimer::release() {
  ChronoEntity* timer = m_timer;
  m_timer = nullptr;
  return timer;
}

ChronoEntity* ActiveTimer::timer() const {
  return m_timer;
}

ChronoEntity* ActiveTimer::held() const {
  return m_held;
}
