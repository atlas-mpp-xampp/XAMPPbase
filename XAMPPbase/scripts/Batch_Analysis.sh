#!/bin/bash
if [ -f "${ClusterControlModule}" ]; then
    source ${ClusterControlModule}
fi
TASK_ID=`get_task_ID`

echo "Sleep for 5 seconds to avoid crashes on shared file systems"
sleep 5

# some initial output
echo "###############################################################################################"
echo "					 Environment variables"
echo "###############################################################################################"
export
echo "###############################################################################################"
echo " "
if [ -z "${ATLAS_LOCAL_ROOT_BASE}" ];then    
    echo "###############################################################################################"
    echo "                    Setting up the environment"
    echo "###############################################################################################"
    # check if TMPDIR exists or define it as TMP
    [[ -d "${TMPDIR}" ]] || export TMPDIR=${TMP}
    echo "cd ${TMPDIR}"
    cd ${TMPDIR}
    echo "export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase"
    export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase    
    echo "Setting up the ATLAS environment:"
    echo "source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh" 
    source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
    echo "Setup athena:"
    echo "asetup ${OriginalProject},${OriginalPatch},${OriginalVersion}"
    asetup ${OriginalProject},${OriginalPatch},${OriginalVersion}
    #Check whether we're in release 21
    if [ -f ${OriginalArea}/../build/${BINARY_TAG}/setup.sh ]; then
        echo "source ${OriginalArea}/../build/${BINARY_TAG}/setup.sh"
        source ${OriginalArea}/../build/${BINARY_TAG}/setup.sh
        WORKDIR=${OriginalArea}/../build/${BINARY_TAG}/bin/
    elif [ -f ${OriginalArea}/../build/${WorkDir_PLATFORM}/setup.sh ];then
        echo "source ${OriginalArea}/../build/${WorkDir_PLATFORM}/setup.sh"
        source ${OriginalArea}/../build/${WorkDir_PLATFORM}/setup.sh
        source ${OriginalArea}/../build/${WorkDir_PLATFORM}/setup.sh
        WORKDIR=${OriginalArea}/../build/${WorkDir_PLATFORM}/bin/
     elif [ -f ${OriginalArea}/../build/${AthAnalysis_PLATFORM}/setup.sh ];then
            echo "source ${OriginalArea}/../build/${AthAnalysis_PLATFORM}/setup.sh"
            source ${OriginalArea}/../build/${AthAnalysis_PLATFORM}/setup.sh        
            WORKDIR=${OriginalArea}/../build/${AthAnalysis_PLATFORM}/bin/
    elif [ -f ${OriginalArea}/../build/${LCG_PLATFORM}/setup.sh ];then
            echo "source ${OriginalArea}/../build/${LCG_PLATFORM}/setup.sh"
            source ${OriginalArea}/../build/${LCG_PLATFORM}/setup.sh
            WORKDIR=${OriginalArea}/../build/${LCG_PLATFORM}/bin/            
    elif [ -z "${CMTBIN}" ];then
        source  ${OriginalArea}/../build/x86_64*/setup.sh
        if [ $? -ne 0 ];then
            echo "Something strange happens?!?!?!"
            export
            echo " ${OriginalArea}/../build/${BINARY_TAG}/setup.sh"
            echo " ${OriginalArea}/../build/${WorkDir_PLATFORM}/setup.sh"            
            send_to_failure        
            exit 100
        fi
    fi
fi

To_Process=""
if [ -f "${InCfg}" ];then
    echo "To_Process=`sed -n \"${TASK_ID}{p;q;}\" ${InCfg}`"
    To_Process=`sed -n "${TASK_ID}{p;q;}" ${InCfg}`
fi
OutFile=""
if [ -f "${OutCfg}" ];then
    echo "OutFile=`sed -n \"${TASK_ID}{p;q;}\" ${OutCfg}`"
    OutFile=`sed -n "${TASK_ID}{p;q;}" ${OutCfg}`
fi

# Process job
cd ${TMPDIR}
echo "execute jobOptions..."
echo "athena ${Execute} ${To_Process} -  --parseFilesForPRW ${Options}"
athena ${Execute}  ${To_Process} - ${Options}  --parseFilesForPRW  &> ${TMPDIR}/log.log
if [ $? -eq 0 ]; then
    cat ${TMPDIR}/log.log
	echo "###############################################################################################"
	echo "						Analysis job terminated successfully"
	echo "###############################################################################################"
	ls -lh
	echo "${TMPDIR}/AnalysisOutput.root ${OutFile}"
	mv ${TMPDIR}/AnalysisOutput.root ${OutFile}
	
else
	cat ${TMPDIR}/log.log
	echo "###############################################################################################"
	echo "					Analysis job has experienced an error"
	echo "###############################################################################################"
	send_to_failure
	exit 100
fi
